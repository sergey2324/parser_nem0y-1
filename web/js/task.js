$(function () {
    $(".deleteShow").on('click', function (e) {
        e.preventDefault();
        let a = $(this).attr('data');
        $.ajax({
            type: "POST",
            url: "/applications/remove",
            dataType : "json",
            data:{
                id: a,
            },
            error: function() {alert("Something wrong");},
            success: function(answer){
                $('.id_'+answer.idDelete).remove();
            }
        });

    });
    $(".editShow").on('click', function () {
        $('#applications-name').val($(this).attr('name'));
        $('#applications-app_id').val($(this).attr('app_id'));
        $('#applications-network_id').val($(this).attr('network_id'));
        $('#applications-user_id').val($(this).attr('user_id'));
        $('#applications-proxy').val($(this).attr('proxy'));
        $('#applications-secret_key').val($(this).attr('secret_key'));
    });
});