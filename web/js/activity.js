/** Здесь собраны активности сайта, различные мелкие скрипты ради которых не имеет смысла создавать фаил

/**
 *
 */
$(window).on('load', function() {
    $('.publish-preview-content').readmore(
        {
            speed: 200,
            maxHeight: 75,
            heightMargin: 16,
            moreLink: '<i class="fa fa-chevron-down publish_arrow_deploy" aria-hidden="true" style="margin-left: 49%; margin-top: 2rem"></i>',
            lessLink: '<i class="fa fa-chevron-up publish_arrow_deploy" aria-hidden="true" style="margin-left: 49%; margin-top: 2rem"></i>'
        });
});