<?php

use yii\db\Migration;

/**
 * Class m190904_175020_add_user_avatar
 */
class m190904_175020_add_user_avatar extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'avatar', $this->string()->defaultValue('avatar.jpg'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'avatar');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190904_175020_add_user_avatar cannot be reverted.\n";

        return false;
    }
    */
}
