<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%audio}}`.
 */
class m190903_124127_create_audio_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('temp_audio', [
            'id' => $this->primaryKey(),
            'id_post_intermediate' => $this->integer(),
            'artist' => $this->string(),
            'title' => $this->string(),
            'duration' => $this->integer(),
            'url' => $this->string(),


        ]);
        $this->createTable('media_content_audio', [
            'id' => $this->primaryKey(),
            'id_post_final' => $this->integer(),
            'artist' => $this->string(),
            'title' => $this->string(),
            'duration' => $this->integer(),
            'url' => $this->string(),

        ]);

        $this->createIndex("temp_audio_id_post_idx", 'temp_audio', ['id_post_intermediate']);
        $this->createIndex("media_content_audio_id_post_idx", 'media_content_audio', ['id_post_final']);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('temp_audio');
        $this->dropTable('media_content_audio');
    }
}
