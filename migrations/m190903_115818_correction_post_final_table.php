<?php

use yii\db\Migration;

/**
 * Class m190903_115818_correction_post_final_table
 */
class m190903_115818_correction_post_final_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('post_final', 'id_vk', 'string');
        $this->addColumn('post_final', 'owner_id', 'string');
        $this->addColumn('post_final', 'date', 'string');
        $this->addColumn('post_final', 'marked_as_ads', 'integer');
        $this->addColumn('post_final', 'post_type', 'string');
        $this->addColumn('post_final', 'id_group', 'string');
        $this->dropColumn('post_final', 'image');
        $this->dropColumn('post_final', 'video');
        $this->db->createCommand("ALTER TABLE {{%post_final}} MODIFY [[text]] longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci")->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('post_final', 'id_vk');
        $this->dropColumn('post_final', 'owner_id');
        $this->dropColumn('post_final', 'date');
        $this->dropColumn('post_final', 'marked_as_ads');
        $this->dropColumn('post_final', 'post_type');
        $this->dropColumn('post_final', 'id_group');
        $this->addColumn('post_final', 'image','string');
        $this->addColumn('post_final', 'video','string');
    }


}
