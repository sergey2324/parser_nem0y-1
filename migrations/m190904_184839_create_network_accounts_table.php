<?php

use yii\db\Migration;

/**
 * Handles the creation of table `network_accounts`.
 */
class m190904_184839_create_network_accounts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('network_accounts', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'access_token' => $this->string(),
            'id_network' => $this->integer(),
            'sm_id' => $this->integer(),
            'account_url' => $this->string(),
        ]);
        $this->addColumn('groups', 'id_account', 'integer');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('network_accounts');
        $this->dropColumn('groups', 'id_account');
    }
}
