<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%video}}`.
 */
class m190903_124518_create_video_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('temp_video', [
            'id' => $this->primaryKey(),
            'id_post_intermediate' => $this->integer(),
            'title' => $this->string(),
            'duration' => $this->string(),
            'description' => $this->string(),
            'track_code' => $this->string(),
            'photo_130' => $this->string(),
            'photo_320' => $this->string(),
            'photo_800' => $this->string(),
            'photo_1280' => $this->string(),
            'first_frame_130' => $this->string(),
            'first_frame_160' => $this->string(),
            'first_frame_320' => $this->string(),
            'first_frame_720' => $this->string(),
            'first_frame_800' => $this->string(),
            'first_frame_1024' => $this->string(),
            'first_frame_1280' => $this->string(),
            'first_frame_4096' => $this->string(),


        ]);
        $this->createTable('media_content_video', [
            'id' => $this->primaryKey(),
            'id_post_final' => $this->integer(),
            'title' => $this->string(),
            'duration' => $this->string(),
            'description' => $this->string(),
            'track_code' => $this->string(),
            'photo_130' => $this->string(),
            'photo_320' => $this->string(),
            'photo_800' => $this->string(),
            'photo_1280' => $this->string(),
            'first_frame_130' => $this->string(),
            'first_frame_160' => $this->string(),
            'first_frame_320' => $this->string(),
            'first_frame_720' => $this->string(),
            'first_frame_800' => $this->string(),
            'first_frame_1024' => $this->string(),
            'first_frame_1280' => $this->string(),
            'first_frame_4096' => $this->string(),

        ]);
        $this->alterColumn('temp_video', 'description', 'longtext');
        $this->alterColumn('media_content_video', 'description', 'longtext');

        $this->createIndex("temp_video_id_post_idx", 'temp_video', ['id_post_intermediate']);
        $this->createIndex("media_content_video_id_post_idx", 'media_content_video', ['id_post_final']);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('temp_video');
        $this->dropTable('media_content_video');
    }
}
