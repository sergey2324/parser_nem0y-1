<?php

use yii\db\Migration;

/**
 * Class m190903_114030_correction_post_intermediate_table
 */
class m190903_114030_correction_post_intermediate_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('post_intermediate', 'image');
        $this->dropColumn('post_intermediate', 'video');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('post_intermediate', 'image','string');
        $this->addColumn('post_intermediate', 'video','string');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190903_114030_correction_post_intermediate_table cannot be reverted.\n";

        return false;
    }
    */
}
