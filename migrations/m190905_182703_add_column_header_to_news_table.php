<?php

use yii\db\Migration;

/**
 * Class m190905_182703_add_column_header_to_news_table
 */
class m190905_182703_add_column_header_to_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('news', 'header', 'string');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('news', 'header');
    }


}
