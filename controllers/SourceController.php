<?php
/**
 * 29.08.2019
 * 20:28
 */

namespace app\controllers;


use app\models\filters\PostIntermediateSearch;
use app\models\ReplacePosts;
use app\models\Source;
use yii\data\ActiveDataProvider;
use yii\web\AssetBundle;
use yii\web\Controller;
use app\models\tables\PostIntermediate;

class SourceController extends Controller
{
    public function actionIndex()
    {
        \Yii::$app->request->getCsrfToken();

        $source = new Source();
        $post = \Yii::$app->request->post();
        $message = 0;

        if (isset($post['groupUrl'])) {

            $groupUrl = $post['groupUrl'];
            $postsNumber = $post['selector'];

            $message = $source->getPosts($groupUrl, $postsNumber);

        }

        //generates numbers for selector on a page
        $selectors = $source->generatePostsNumber();

        $query = PostIntermediate::find();

        $provider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort' => [
                    'defaultOrder' => [
                        'id' => SORT_DESC,
                    ]
                ],
                'pagination' => false,
            ]
        );

        return $this->render('posts.php', [
            'selectors' => $selectors,
            'message' => $message,
            'provider' => $provider
        ]);
    }

    public function actionKeys()
    {
        if (\Yii::$app->request->isAjax) {

           $keyList = \Yii::$app->request->post('keylist');
            (new ReplacePosts())->start($keyList);

        }
        $this->redirect('/publish');
    }

}