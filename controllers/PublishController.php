<?php
/**
 * Created by PhpStorm.
 * User: NEMOY
 * Date: 03.09.2019
 * Time: 17:35
 */

namespace app\controllers;


use app\models\ReplacePosts;
use app\models\tables\PostFinal;
use app\models\tables\TempPhoto;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class PublishController extends Controller
{
    public function actionIndex()
    {

        $dataProvider = (new PostFinal())->providerFinalPosts();

        $addedPosts = ReplacePosts::$postsSaved;

        return $this->render('index.php', [
            'dataProvider' => $dataProvider,
            'addedPosts' => $addedPosts,
        ]);
    }


    public function actionDelete()
    {
        $user = \Yii::$app->user->identity->getId();

        if (\Yii::$app->request->isAjax) {
           foreach(PostFinal::findAll(['id_user' => $user]) as $one){
               TempPhoto::deleteAll(['id_post_intermediate' => $one->id]);
           }
            PostFinal::deleteAll(['id_user' => $user]);

            return 'Все посты удалены';
        }
    }
}