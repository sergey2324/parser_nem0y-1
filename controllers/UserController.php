<?php

namespace app\controllers;

use Yii;
//use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\LoginForm;
use yii\web\Response;
//use yii\filters\VerbFilter;
use app\models\SignupForm;
use yii\web\UploadedFile;
use app\models\User;
//use yii\web\NotFoundHttpException;

class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
           /* 'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],*/
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
      public function actionLogout()
      {
          Yii::$app->user->logout();

          return $this->goHome();
      }

    /**
     * Displays create page.
     *
     * @return string
     */
    public function actionSignup()
    {
        Yii::$app->name = 'Регистрация';
        $buttonName = 'Зарегистрироваться';

        $model = new SignupForm();

        if(isset($_POST['SignupForm'])){
            $model->attributes = $_POST['SignupForm'];
            $model->avatar = UploadedFile::getInstance($model,'avatar');

            if($model->signup()){
                $model->upload();
                Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
                return $this->goHome();
            }
        }

        /* раньше было
         * if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            $model->avatar = UploadedFile::getInstance($model, 'avatar');
            $model->upload();
            Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
            return $this->goHome();
        }*/

        $title = 'Регистрация';
        $registration = true;
        return $this->render('signup', [
            'title' => $title,
            'model' => $model,
            'buttonName' => $buttonName,
            'registration' => $registration,
        ]);
    }

    /**
     * Profile page.
     * @param integer $id
     * @return Response|string
     */
    public function actionProfile($id)
    {
        Yii::$app->name = 'Профиль';
        $user = new User;
        $model = $user->findIdentity($id);
        $title = 'Мой профиль';
        return $this->render('profile', [
            'title' => $title,
            'model' => $model,
        ]);
    }


    /**
     * Delete page.
     * @param integer $id
     * @return string
     */
    public function actionDelete($id)
    {
        //$user = new User;
        $title = 'Удаление пользователя';

        return $this->render('delete_user', [
            'title' => $title,
            'user_id' => $id,
        ]);
    }

    /**
    * Delete user action.
    * @param integer $id
    * @return string
    */
    public function actionDelete_user($id)
    {
        $user = new User;

        if($user->deleteUser($id)) {
            Yii::$app->session->setFlash('success', 'Ваш аккаунт удален.');
            return $this->goHome();
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'profile' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate_information($id)
    {
        Yii::$app->name = 'Профиль';
        $title = 'Изменение профиля';
        $buttonName = 'Сохранить изменения';

        $user = new User;
        $model = $user->findIdentity($id);


        if(isset($_POST['User'])) {

            $answer = $model->selectChange($_POST['User']);

            if($answer === 'success') {
                $model->updateUser($_POST['User'], $_FILES['User']['name']['avatar']);
                Yii::$app->session->setFlash('success', 'Изменение ваших данных прошло успешно.');

                return $this->redirect(['profile',
                'id' => $model->id,
                'model' => $model,
            ]);
            } else {
                $model = $answer;
            }
        }

        $registration = false;
        return $this->render('signup', [
            'title' => $title,
            'model' => $model,
            'buttonName' => $buttonName,
            'registration' => $registration,
        ]);
    }

    /**
     * Change password action.
     * If update is successful, the browser will be redirected to the 'profile' page.
     * @param integer $id
     * @return string
     */
    public function actionUpdate_password($id) {

        $model = new User;
        $model->scenario = User::SCENARIO_UPDATE_PASSWORD;
        if(isset($_POST['User'])) {
            $model->setAttributes($_POST['User']);
            if($model->validate()) {
                $newUser = $model->updatePassword($id);

                Yii::$app->session->setFlash('success', 'Изменение вашего пароля прошло успешно.');

                return $this->redirect(['profile',
                    'id' => $newUser->id,
                    'model' => $newUser,]);
            }
        }

        return $this->render('password', [
            'model' => $model,
        ]);
    }
}