<?php
use yii\helpers\Url;

/** @var $model \app\models\tables\News */
?>


        <div class="news-preview-header"><a href="<?= Url::to(['news/view', 'id' => $model->id]) ?>"><h4><?= $model->header ?></h4></a></div>
        <div class="news-preview-content"><?= $model->text?></div>
        <div class="news-preview-date">Автор: <?= $model->author ?> / Опубликовано: <?= $model->date_create?>
            <a href="<?= Url::to(['news/update', 'id' => $model->id]) ?>"> Редактировать</a>
            <a href="<?= Url::to(['news/delete', 'id' => $model->id]) ?>"> Удалить</a>
        </div>
        <hr>

