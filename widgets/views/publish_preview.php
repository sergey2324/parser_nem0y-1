<?php
use yii\bootstrap\Modal;
use \yii\bootstrap\Carousel;

echo \acerix\yii2\readmore\Readmore::widget();
?>

<div class="publish-preview-post">

<div class="publish_post_header">
    <div>    <label style="padding-bottom: 10px" title="Выбрать пост">
            <input type="checkbox" class="option-input checkbox" />
        </label></div>
    <div class="btn-group"  role="group" aria-label="Basic example">
        <button type="button" title="Загрузить пост" class="btn btn_posts btn-secondary"><i class="fa fa-arrow-circle-up" aria-hidden="true"></i></button>
        <button type="button" title="Запланировать загрузку" class="btn btn_posts btn-secondary"><i class="fa fa-clock-o" aria-hidden="true"></i></button>
        <?php Modal::begin([
            'toggleButton' => [
                'tag' => 'button',
                'type' => 'button',
                'title' =>'Редактировать пост',
                'class' => 'btn btn_posts btn-secondary',
                'label' => '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>'],
            'header' => 'Редактирование',
            'footer' => 'Низ окна',
        ]);
        Modal::end();?>
            <?php Modal::begin([
                'toggleButton' => [
                        'tag' => 'button',
                        'type' => 'button',
                        'title' =>'Просмотр',
                        'class' => 'btn btn_posts btn-secondary',
                        'label' => '<i class="fa fa-eye" aria-hidden="true"></i>'],
                'header' => '',
                'footer' => '
                    <div class="publish_post_statBlock" style="padding: 2rem; padding-bottom: 0; float: right;">
                        <div class="publish_post_statBlock__date">
                                <span>99 шлёптября 2007г</span>
                        </div>
                        <div class="publish_post_statBlock__stat">
                             <i class="fa fa-thumbs-o-up" aria-hidden="true"></i><span> 9999</span>
                             <i class="fa fa-share-square-o" aria-hidden="true"></i><span> 9999</span>
                             <i class="fa fa-comments-o" aria-hidden="true"></i><span> 9999</span>
                         </div>
                    </div>
                ',
                'closeButton' => ['label' => 'Закрыть']
            ]);?>
        <?php if(isset($photo)):?>
            <div class="publish-preview-image">
                <?php echo Carousel::widget([
                    'items' => $carouselItems,
                    'options' => [
                        'style' => 'width:100%'
                    ]
                ]);
                ?></div>
        <? else:?>
            <div class="publish-preview-image"><img src="/img/no_photo.jpg" alt="No Image" style="width: 100%; "> <hr></div>
        <? endif;?>
        <div class="publish-preview-content"><?= $model->text?></div>
           <?php Modal::end();?>
        <button type="button" title="Копировать пост" class="btn btn_posts btn-secondary"><i class="fa fa-files-o" aria-hidden="true"></i></button>
        <button type="button" title="Удалить пост" class="btn btn_posts btn-secondary"><i class="fa fa-trash" aria-hidden="true"></i></button>

    </div>
</div>


    <?php if(isset($photo)):?>
    <div class="publish-preview-image"><img src="<?= $photo?>" alt="VK_Photo" style="width: 100%"> <hr></div>
    <? else:?>
    <div class="publish-preview-image"><img src="/img/no_photo.jpg" alt="No Image" style="width: 100%; "> <hr></div>
    <? endif;?>
    <div class="publish-preview-content"><?= $model->text?> </div>


    <!-- TODO оставил статистику на будущее, так же этот код есть в модальном окне
    <div class="publish_post_statBlock" style="padding: 2rem; padding-bottom: 0; float: right;">
        <div class="publish_post_statBlock__date">
            <span>99 шлёптября 2007г</span>
        </div>
        <div class="publish_post_statBlock__stat">
            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i><span> 9999</span>
            <i class="fa fa-share-square-o" aria-hidden="true"></i><span> 9999</span>
            <i class="fa fa-comments-o" aria-hidden="true"></i><span> 9999</span>

        </div>
    </div>
-->
</div>
