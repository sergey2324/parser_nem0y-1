<?php


namespace app\widgets;


use app\models\Applications;
use yii\base\Widget;

class CardWidget extends Widget
{

    public $model;
    public $linked = false;

    public function run()
    {
        if(is_a($this->model,Applications::class)){
            return $this->render('card', ['model' => $this->model,'linked' => $this->linked]);
        }
        throw new \Exception("Модель должна быть класса Application!");

    }

}