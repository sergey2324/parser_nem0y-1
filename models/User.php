<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\web\UploadedFile;
//use mohorev\file\UploadImageBehavior;

/**
 * This is the model class for table "user".
 * @property int $id
 * @property string $username
 * @property string $email
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $verification_token
 * @property string $avatar
 * @property integer $status
 */

class User extends ActiveRecord implements IdentityInterface
{
    private $password;
    private $passwordNew;

    const SCENARIO_UPDATE_LOGIN = 'update_login';
    const SCENARIO_UPDATE_EMAIL = 'update_email';
    const SCENARIO_UPDATE_PASSWORD = 'update_password';

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    public function rules()
    {
        return [
            [['username', 'email'], 'trim'],

            [['username'], 'unique', 'targetClass' => '\app\models\User',
                'message' => 'This username has already been taken.', 'on' =>[self::SCENARIO_UPDATE_LOGIN]],
            ['username', 'string', 'min' => 4, 'max' => 255],

            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            [['email'], 'unique', 'targetClass' => '\app\models\User',
                'message' => 'This email address has already been taken.', 'on' =>[self::SCENARIO_UPDATE_EMAIL]],

            [['passwordNew', 'password'], 'required', 'on' => [self::SCENARIO_UPDATE_PASSWORD]],
            ['password', 'string', 'min' => 6, 'max' => 255, 'on' => [self::SCENARIO_UPDATE_PASSWORD]],
            ['password', 'validateOldPassword', 'on' => [self::SCENARIO_UPDATE_PASSWORD] ],
            ['passwordNew', 'string', 'min' => 6, 'max' => 255, 'on' => [self::SCENARIO_UPDATE_PASSWORD]],

            [['avatar'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, png, jpeg',
                'maxSize' => 30 * 1024, 'tooBig' => 'Максимальный размер аватара 30KB'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Логин',
            'avatar' => 'Аватар',
            'password' => 'Пароль',
            'auth_key' => 'Auth key',
            'password_hash' => 'Password hash',
            'password_reset_token' => 'Password reset token',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created at',
            'updated_at' => 'Updated at',
            'verification_token' => 'Verification token',
            'passwordNew' => 'Новый пароль',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
    /* функция из модели User по умолчанию от Yii
     * public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }
        return null;
    }*/

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }
    /**
     * {@inheritdoc}
     */
    public function getPasswordNew()
    {
        return $this->passwordNew;
    }
    /**
     * {@inheritdoc}
     */
    public function setPasswordNew($passwordNew)
    {
        $this->passwordNew = $passwordNew;
    }
    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {
        return $this->password;
    }
    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     * @param string $password
     * @throws
     */
    public function setPassword($password)
    {
        $this->password = $password;
        if($password) {
            $this->password_hash = Yii::$app->security->generatePasswordHash($password);
        }
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Generates new email verification token
     */
    public function generateEmailVerificationToken()
    {
        $this->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }


    /**
     * @param bool $insert whether this method called while inserting a record.
     * If `false`, it means the method is called while updating a record.
     * @return bool whether the insertion or updating should continue.
     * If `false`, the insertion or updating will be cancelled.
     **/
    public function beforeSave($insert)
    {
        if(!parent::beforeSave($insert)) {
            return false;
        }
        if($insert) {
            $this->generateAuthKey();
        }
        return true;
    }

    /**
     * Delete user account
     * @param integer $id
     * @return true
     */
    public function deleteUser($id) {

        $user = $this->findIdentity($id);

        $user->status = self::STATUS_DELETED;
        $user->save();

        return true;
    }

    /**
     * Select changed user information.
     * @var array $oldUser[]
     * @return string
     */
    public function selectChange($newUser)
    {
        $myNewUser = new User;

        if($this->username !== $newUser[username]) {
            $myNewUser->scenario = self::SCENARIO_UPDATE_LOGIN;
            $myNewUser->username = $newUser[username];
        } else {
            $myNewUser->username = $this->username;
        }

        if($this->email !== $newUser[email]) {
            $myNewUser->scenario = self::SCENARIO_UPDATE_EMAIL;
            $myNewUser->email = $newUser[email];
        } else {
            $myNewUser->email = $this->email;
        }

        if (!$myNewUser->validate()) {
            return $myNewUser;
        } else {
            return 'success';
        }
    }

    /**
     * Update user information.
     * @var array $newUser[]
     * @var string $userImage
     * @return string
     */
    public function updateUser ($newUser, $userImage) {
        $this->username = $newUser[username];
        $this->email = $newUser[email];

        if($userImage !== '') {
            $this->avatar = $newUser[avatar];
            $this->updateAvatar();
        }
        return $this->save();
    }

    /**
     * Update user avatar.
     */
    public function updateAvatar() {
        $this->avatar = UploadedFile::getInstance($this,'avatar');

        if(!is_null($this->avatar)) {
            $path =  Yii::getAlias('@app/web') . '/upload/user/' .
                $this->avatar->getBaseName() . '.' . $this->avatar->getExtension();
            $this->avatar->saveAs($path);
            $this->avatar = $this->avatar->getBaseName() . '.' . $this->avatar->getExtension();;
        }
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     */
    public function validateOldPassword($attribute)
    {
        $user = $this->findByUsername(Yii::$app->user->identity->username);;

        if (!$user->validatePassword($this->password)) {
            $this->addError($attribute, 'Введен неверный пароль.');
        }
    }

    /**
     * Update user password.
     * @var $id int
     * @return User
     */
    public function updatePassword ($id) {
        $user = $this->findIdentity($id);
        $user->setPassword($this->passwordNew);
        $user->save();
        return $user;
    }
}
