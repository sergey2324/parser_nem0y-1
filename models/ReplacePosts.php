<?php
/**
 * 06.09.2019
 * 22:39
 */

namespace app\models;


use app\models\tables\PostFinal;
use app\models\tables\PostIntermediate;
use app\models\tables\TempPhoto;
use yii\base\Model;

class ReplacePosts extends Model
{
    static $postsSaved = 0;

    public $relatedTables = [
        'temp_photo',
        'temp_video',
        'temp_audio',
    ];

    public function start($keyList)
    {
        $postsToKeep = $this->getPosts($keyList);
        $this->reSavePosts($postsToKeep);
        $this->clearPostsIntermediateTable();
        $this->clearRelatedMedia($keyList, $this->relatedTables);

        return static::$postsSaved;
    }

    public function getPosts($keyList)
    {
        $postsIntermediate = PostIntermediate::find()
            ->where(['id' => $keyList])
            ->all();

        return $postsIntermediate;
    }

    public function reSavePosts($postsToKeep)
    {
        foreach ($postsToKeep as $postIntermediate) {

            $postFinal = new PostFinal();

            $postFinal->id = $postIntermediate->id;
            $postFinal->attributes = $postIntermediate->attributes;
            $postFinal->id_user = \Yii::$app->user->identity->getId();

            if($postFinal->save()) {
                static::$postsSaved++;
            } else {
                echo 'Post with id ' . $postIntermediate->id . ' wasn\'t saved';
                return false;
                //TODO: throw an Exception
            }
        }
        return static::$postsSaved;
    }

    public function clearPostsIntermediateTable()
    {
        PostIntermediate::deleteAll();
        //TODO delete all by account_id
    }

    public function clearRelatedMedia($keyList, $tablesNames)
    {

        $notInList = implode(', ', $keyList);

        foreach ($tablesNames as $tablesName) {
            $this->deleteFromTable($tablesName, $notInList);
        }
    }

    public function deleteFromTable($tableName, $notInList)
    {
        $sql = "delete from `$tableName` where `id_post_intermediate` not in ($notInList)";

        \Yii::$app->db->createCommand($sql)
            ->execute();
    }
}