<?php
namespace app\models;
use Yii;
use yii\base\Model;
use app\models\User;
use yii\web\UploadedFile;
/**
 * Signup form
 */
class SignupForm extends Model
{
    public $id;
    public $username;
    public $email;
    public $password;

    /**
     * @var UploadedFile
     */
    public $avatar;

    const AVATAR_DEFAULT = 'avatar.jpg';

    public function rules()
    {
        return [
            [['username', 'email'], 'trim'],
            [['username', 'email', 'password'], 'required'],

            ['username', 'unique', 'targetClass' => '\app\models\User',
                'message' => 'Пользователь с таким логином уже зарегистрирован.'],
            ['username', 'string', 'min' => 4, 'max' => 255],

            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\User',
                'message' => 'Пользователь с таким email уже зарегистрирован.'],

            ['password', 'string', 'min' => 6, 'max' => 255],

            [['avatar'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, png, jpeg',
                'maxSize' => 30 * 1024, 'tooBig' => 'Максимальный размер аватара 30KB'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'avatar' => 'Аватар',
            'password' => 'Пароль',
        ];
    }
    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->id = $this->id;
        $user->username = $this->username;
        if(is_null($this->avatar)) {
            $user->avatar = self::AVATAR_DEFAULT;
        } else {
            $user->avatar =  $this->avatar;
        }
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        return $user->save(); //&& $this->sendEmail($user);
    }

    public function upload()
    {
        if(!is_null($this->avatar)) {
            $path =  Yii::getAlias('@app/web') . '/upload/user/' .
                $this->avatar->getBaseName() . '.' . $this->avatar->getExtension();
            $this->avatar->saveAs($path);
        }
        return true;
    }



    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    /*protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
    }*/
}