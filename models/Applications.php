<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "applications".
 *
 * @property int $id
 * @property string $name
 * @property int $app_id
 * @property string $secret_key
 * @property string $proxy
 * @property int $network_id
 * @property int $user_id
 */
class Applications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'applications';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['app_id', 'network_id', 'user_id'], 'integer'],
            [['name', 'proxy'], 'string', 'max' => 255],
            [['secret_key'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'app_id' => 'App ID',
            'secret_key' => 'Secret Key',
            'proxy' => 'Proxy',
            'network_id' => 'Network ID',
            'user_id' => 'User ID',
        ];
    }
}
