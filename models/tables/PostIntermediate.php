<?php

namespace app\models\tables;

use Yii;

/**
 * This is the model class for table "post_intermediate".
 *
 * @property int $id
 * @property string $text
 * @property string $tags
 * @property string $url_group
 * @property string $id_vk
 * @property string $owner_id
 * @property string $date
 * @property int $marked_as_ads
 * @property string $post_type
 * @property string $id_group
 * @property string $photos
 * 
 */
class PostIntermediate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post_intermediate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['marked_as_ads'], 'integer'],
            [['tags', 'url_group', 'id_vk', 'owner_id', 'date', 'post_type', 'id_group'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Text',
            'tags' => 'Tags',
            'url_group' => 'Url Group',
            'id_vk' => 'Id Vk',
            'owner_id' => 'Owner ID',
            'date' => 'Date',
            'marked_as_ads' => 'Marked As Ads',
            'post_type' => 'Post Type',
            'id_group' => 'Id Group',
        ];
    }
    

    public function getTemp_video()
    {
        return $this->hasMany(TempVideo::className(), ['id_post_intermediate' => 'id']);


    }

    public function getTemp_audio()
    {
        return $this->hasMany(TempAudio::className(), ['id_post_intermediate' => 'id']);

    }

    public function getPhotos()
    {
        return $this->hasMany(TempPhoto::class, ['id_post_intermediate' => 'id']);
        
    }
}
