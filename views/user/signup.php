
<?
/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $title string */
/* @var $buttonName string */
/* @var $registration bool*/
?>
<div class="user-create">

    <h3> <?= $title ?> </h3>

    <?= $this->render('_form', [
        'model' => $model,
        'buttonName' => $buttonName,
        'registration' => $registration,
    ]) ?>

</div>