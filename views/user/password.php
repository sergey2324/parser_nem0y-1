<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $buttonName string */
/* @var $registration bool */
?>
<div class = 'site-signup'>

    <p> Для изменения пароля введите, пожалуйста, текущий и новый пароли: </p>

    <div class = 'row'>
        <div class = 'col-lg-5'>
            <?php $form = \yii\bootstrap\ActiveForm::begin([
                    'id' => 'form-signup',
                    'options' => ['enctype' => 'multipart/form-data'],
                    'layout' => 'horizontal',
                    'fieldConfig' => ['horizontalCssClasses' =>
                    ['label' => 'col-sm-2',]],
            ]); ?>

            <?= $form->field($model, 'password')->passwordInput()?>

            <?= $form->field($model, 'passwordNew')->passwordInput()?>

            <?= Html::submitButton('Изменить пароль', ['class' => 'btn btn-primary',
                'name' => 'password-button']) ?>

            <?php \yii\bootstrap\ActiveForm::end(); ?>
        </div>
    </div>
</div>