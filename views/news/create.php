<?php
$this->title = 'Создание новости';
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\tables\Tasks */
/* @var $form yii\widgets\ActiveForm */


?>

<div class="news_one_main row" style="width: 50%">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-lg-12">
        <?= $form->field($model, 'header')->textInput(['maxlength' => 100 ]) ?>
    </div>
    <div class="col-lg-12 news_text_area" style="min-height: 200px">
        <?= $form->field($model, 'text')->textarea(['minheight' => 2000]) ?>
    </div>
    <div class="col-lg-12">
        <?= $form->field($model, 'author')->textInput() ?>
    </div>

    <div class="col-lg-12">
        <?= $form->field($model, 'date_create')
            ->textInput(['type' => 'date'])
        ?>
    </div>

</div>
    <?= Html::submitButton("Сохранить", ['class' => 'btn btn-success']); ?>
    <?php ActiveForm::end(); ?>

