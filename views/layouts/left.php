<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?php if(!Yii::$app->user->isGuest):?>
                <img src="<?= '/upload/user/' .
                Yii::$app->user->identity->avatar ?>" class="img-circle" alt="User"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->username ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
            <?php endif;?>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'МЕНЮ', 'options' => ['class' => 'header']],
                    ['label' => 'Новости', 'icon' => 'newspaper-o', 'url' => ['/news']],

                    !Yii::$app->user->isGuest ? (
                    ['label' => 'Параметры', 'icon' => 'file-code-o', 'url' => ['/applications']]
                    ):(['label' => '']),
                    !Yii::$app->user->isGuest ? (
                    ['label' => 'Источники', 'icon' => 'dashboard', 'url' => ['/source']]
                    ):(['label' => '']),
                    !Yii::$app->user->isGuest ? (
                    ['label' => 'Публикации', 'icon' => 'dashboard', 'url' => ['*'],
                        'items' => [
                            ['label' => 'Ожидающие', 'icon' => 'file-code-o', 'url' => ['/publish'],],
                            ['label' => 'Архив', 'icon' => 'dashboard', 'url' => ['/archive'],],]
                    ]
                    ):(['label' => '']),
                    !Yii::$app->user->isGuest ? (
                    ['label' => 'Архив', 'icon' => 'dashboard', 'url' => ['/archive']]
                    ):(['label' => '']),
                    Yii::$app->user->can('EditRoles') ? (
                    ['label' => 'Роли', 'icon' => 'dashboard', 'url' => ['/admin']]
                    ):(['label' => '']),
                    Yii::$app->user->isGuest ? (
                    ['label' => 'Профиль', 'icon' => 'id-card-o', 'url' => ['*'], 'visible' => !Yii::$app->user->isGuest]
                    ):(
                    ['label' => 'Профиль', 'icon' => 'id-card-o', 'url' => ['*'],
                        'items' => [
                            Yii::$app->user->isGuest ?: (  ['label' => 'Обо мне', 'icon' => 'user', 'url' => ['/user/profile',
                                'id' =>  Yii::$app->user->identity->id], ]),
                            ['label' => 'Изменить данные', 'icon' => 'edit', 'url' => ['/user/update_information',
                                'id' =>  Yii::$app->user->identity->id], ],
                            ['label' => 'Изменить пароль', 'icon' => 'pencil', 'url' => ['/user/update_password',
                                'id' =>  Yii::$app->user->identity->id], ],
                            ['label' => 'Удалить', 'icon' => 'remove', 'url' => ['/user/delete',
                                'id' =>  Yii::$app->user->identity->id], ],
                        ],
                    ]
                    ),

                    Yii::$app->user->isGuest ? (
                    ['label' => 'Войти', 'icon' => 'sign-in', 'url' => ['/user/login']]
                    ) : (
                    ['label' => 'Выйти', 'icon' => 'sign-out', 'url' => ['/user/logout']]
                    ),

                ],
            ]
        ) ?>

    </section>

</aside>