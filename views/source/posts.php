<?php

/* @var $this yii\web\View */

use app\assets\SourceAsset;
use yii\grid\GridView;
use yii\helpers\Html;

SourceAsset::register($this);

?>


<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= Html::beginForm(['source/'], 'post', ['class' => 'id-group-form', 'data-pjax' => '1',]) ?>

    <?= Html::input('text', 'groupUrl', '', ['class' => 'id-group-input form-control', 'placeholder' => 'Скопируйте сюда url группы']) ?>

    <?= Html::dropDownList('selector', $selectors[100], $selectors, [
        'text' => 'Выберите количество постов',
        'class' => 'form-control',
    ]) ?>

    <?= Html::submitButton('Получить посты', ['class' => 'submit btn btn-outline-secondary']) ?>

    <?= Html::endForm() ?>

    <?= Html::button('Сохранить выбранные посты', ['class' => ['hidden', 'btn'], 'id' => 'savePosts']) ?>


    <div class="posts-grid-view">
        <?php
        if ($message) {

            if (is_numeric($message)) {
                $content = 'Спарсено постов: ' . $message;
                } else {
                $content = $message;
            }

            echo Html::tag('p', $content, ['class' => 'source-add-post-message']);


            //TODO: fix pagination issue - do not accept get-parameters while changing page
            echo GridView::widget(
                [
                    'dataProvider' => $provider,
                    "id" => "grid",
                    'columns' => [
                        [
                            'class' => \yii\grid\CheckboxColumn::class,
                            'options' => [
                                'name' => 'posts',
                            ]
                        ],

                        'url_group',
                        'text',
                        [
                            'attribute' => 'temp_photo',
                            'label' => 'Image',
                            'format' => 'html',

                            'value' => function ($data) {
                                $photos = $data->photos;
                                foreach ($photos as $photoObject) {
                                    $photoSizes = $photoObject->getAttributes($photoObject->photoSizes);

                                    foreach ($photoSizes as $size) {
                                        if ($size) {
                                            $url = $size;
                                        }
                                    }
                                }

                                if (isset($url)) {
                                    return Html::img($url, ['alt' => 'image VK', 'width' => '200']);
                                } else {
                                    return 'No image';}
                            }
                        ],
                    ],
                ]
            );
        }
        ?>

    </div>

</div>
